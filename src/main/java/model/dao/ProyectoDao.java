package model.dao;

//Estructura de datos
import java.util.ArrayList;

import model.vo.Lider;
import model.vo.Proyecto;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//Clase para conexión
import util.JDBCUtilities;

public class ProyectoDao {

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {

        ArrayList<Proyecto> respuesta = new ArrayList<Proyecto>();
        Connection conexion = JDBCUtilities.getConnection();

        try{

            String consulta1 = "SELECT Fecha_Inicio, Numero_Habitaciones, Numero_Banos"+
            "FROM Proyecto p"+
            "WHERE Constructora = 'Pegaso';";
            PreparedStatement statement = conexion.prepareStatement(consulta1);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()){
                Proyecto requerimiento_1= new Proyecto();
                requerimiento_1.setFecha_inicio(resultSet.getString("Fecha_Inicio"));                
                requerimiento_1.setNumero_habitaciones(resultSet.getInt("Numero_Habitaciones"));
                requerimiento_1.setNumero_banos(resultSet.getInt("Numero_Banos"));

                respuesta.add(requerimiento_1);
            }
            resultSet.close();
            statement.close();
        }catch(SQLException e){
            System.err.println("Error consultando: "+e);
        }finally{
            if(conexion != null){
                conexion.close();
            }
        }
        return respuesta;
    }


    public ArrayList<ProyectoDao> query_requerimiento_2() throws SQLException {

            ArrayList<ProyectoDao> respuesta = new ArrayList<ProyectoDao>();
            Connection conexion = JDBCUtilities.getConnection();
    
            try{
    
                String consulta2 = "SELECT p.Fecha_Inicio, p.Numero_Habitaciones, p.Numero_Banos, l.Nombre, l.Primer_Apellido, t.Estrato"+
                "FROM Proyecto p "+
                "INNER JOIN Lider l on l.ID_Lider = p.ID_Lider "+
                "INNER JOIN Tipo t on t.ID_Tipo  = p.ID_Tipo" +
                "WHERE Constructora = 'Pegaso';";
                PreparedStatement statement = conexion.prepareStatement(consulta2);
                ResultSet resultSet = statement.executeQuery();
    
                while(resultSet.next()){
                    ProyectoDao requerimiento_2= new ProyectoDao();
                    requerimiento_2.setFecha_Inicio(resultSet.getString("p.Fecha_Inicio"));                
                    requerimiento_2.setNumero_Habitaciones(resultSet.getString("Numero_Habitaciones"));
                    requerimiento_2.setNombre(resultSet.getString("Nombre"));
                    requerimiento_2.setPrimer_Apellido(resultSet.getString("Primer_Apellido"));
                    requerimiento_2.setEstrato(resultSet.getString("Estrato"));


                    respuesta.add(requerimiento_2);
                }
                resultSet.close();
                statement.close();
            }catch(SQLException e){
                System.err.println("Error consultando: "+e);
            }finally{
                if(conexion != null){
                    conexion.close();
                }
            }
            return respuesta;
        }
        
    }// Fin del método query_requerimiento_2


    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {
        
    }// Fin del método query_requerimiento_3


    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException{ }// Fin del método query_requerimiento_4